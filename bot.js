/*
- Random Bot was created by Eric T. | Copyright (c) 2017

 - You may not claim ownership of this software - all credit goes to myself, EMT#3690 / Eric T., and anyone in the credits.
*/

const Discord = require('discord.js');
const bot = new Discord.Client();
const fs = require("fs");
const config = require('./config.json');
const prefix = (config.prefix);

bot.login(config.botToken);

bot.on('ready', () => {
  console.log("Online!");
  bot.user.setGame('By EMT#3690');
  bot.user.setStatus('dnd');
});

bot.on('disconnected', function () {
    console.log("Offline!");
    process.exit(1);
});

bot.on('message', message => {
  if(message.author.bot) return;


  // Make sure you add links to the links.txt, or this command will throw an error!
  if (message.content === (config.trigger)) {
  fs.readFile("links.txt", "utf8", function(err, data) {
    if (err) return console.error(err);
    var lines = data.split("\r\n");
    var linesTotal = lines.length;
    var lineSend = Math.floor(Math.random() * linesTotal);
    var choice = lines[lineSend];
    message.channel.sendMessage("", {embed:{image:{url: choice}}});
    });
  }

  if (message.content === prefix + 'help') {
    const embed = new Discord.RichEmbed()
    .setColor(0xe81212)
    .setAuthor(`Bot prefix: ${config.prefix}`, bot.user.avatarURL)
    .addField(`❯ ${config.trigger}`, 'Sends a random image from the links file.')
    .addField("❯ ping", "Pong! Returns latency between you and the bot.")
    .addField("❯ 8ball", "Ask the bot anything, and it shall give an answer.")
    .addField("❯ ❯ author", "Stalk me at the provided links please.")
    .addField("❯ credit", "Lists the names of all the contributors to the bots code, or testing.")
    .setFooter('Coded by EMT#3690', bot.user.avatarURL)
    .setTimestamp()
  message.channel.sendEmbed(embed)
  }

  if (message.content === prefix + "ping")
    message.channel.sendMessage(":hourglass:")
    .then(msg => {
    msg.edit(`**Pong!** (took: ${msg.createdTimestamp - message.createdTimestamp}ms) :alarm_clock:`);
  });

  if (message.content.startsWith(prefix + '8ball')) {
    var choices = [/*Yes*/ "Definitely!", "Oh yeah!", "You know it!", /*Maybe*/ "That's a big maybe.", "I don't know about that...", "It's possible...",/*No*/ "Definitely not.", "In your dreams...", "Not gonna happen."];
    var answer = [Math.floor(Math.random() * choices.length)];
  message.channel.sendMessage(`:8ball: ` + choices[answer])
  }

  if (message.content === prefix + 'author') {
    const embed = new Discord.RichEmbed()
    .setColor(0x008080)
    .setAuthor("Find me at any of the links below! <3", bot.user.avatarURL)
    .addField("❯ Twitter", "You can follow me here (please follow me... I'm lonely) -- [Twitter](https://www.twitter.com/iAm_EMT)")
    .addField("❯ Discord", "Add me as a friend (or just message me German shepherds, I'll take either)! -- <@147478852015357952>")
    .addField("❯ Steam", "Also add me on Steam (or don't, I'm not your mom...) -- [Steam](http://steamcommunity.com/id/iAmEMT/)")
    .addField("❯ Discord server", "Join my Discord server here! --")
    .setFooter('Coded by EMT#3690', bot.user.avatarURL)
    .setTimestamp()
  message.channel.sendEmbed(embed)
  message.channel.sendMessage("https://discord.gg/Tv9EcNX")
  }

  if (message.content === prefix + 'credit') {
  const embed = new Discord.RichEmbed()
  .setColor(0xe81212)
  .addField("Creator", "EMT - <@147478852015357952>")
  .addField("This guy", "Curry - <@175751137473986560>")
  .addField("Another guy", "Gido - <@181965297249550336>")
  .addField("This girl", "Savannah - <@205110913416560647>")
  .addField("This guy too", "Peek - <@212630687600214017>")
  .addField("links.txt code from StackOverflow", "[Menteroth Profile](http://stackoverflow.com/users/2840550/menteroth)")
  .setFooter('Coded by EMT#3690', bot.user.avatarURL)
  .setTimestamp()
  message.channel.sendEmbed(embed)
  }

});
