## CREDITS

- © 2017 Eric T. | All rights reserved
- View more credits using the command: `credits`

## INSTALLATION

- In order to make this bot function, you must have the latest versions of: **Node.js (https://nodejs.org/en/download/)**

- Installing the Discord.js library > Open the master folder from GitHub - Shift + Right Click - Open Command Prompt - Execute this command: `npm install discord.js`

- Once the dependencies have been installed, place the bot token (found here: https://discordapp.com/developers/applications/me) in the respected field inside of the config.json file, along with a preffered prefix.

- When that is all completed, simply open a new command prompt in the folder directory and type: `node bot.js` 

- You may change the prefix and trigger in the `config.json` file under their respected fields.

## HOW TO INVITE THE BOT

1. Grab your `Client ID` from the developer applications page (found here: https://discordapp.com/developers/applications/me)
2. Use the handy dandy permissions and invite link generator (found here: https://finitereality.github.io/permissions/?v=0)
3. Select all the permissions you'd like the bot to have, and insert the `Client ID` in the Client ID field
4. Lastly, save the link generated and paste it in your browser. It will bring you to a page where you can view servers the bot can be invited to

## SUPPORT
https://discord.gg/Tv9EcNX

- If you are in need of support, you can join my Discord server and tag me asking for help!
- I will not provide support if you have modified the code in any way.
